from tkinter import *

product_list = ['яблоки', 'груши', 'бананы', 'морковь', 'помидоры', 'картошка', 'хлеб']

def in_listbox():
    select = list(listbox_products.curselection())
    select.reverse()
    for i in select:
        listbox_purchases.insert(END, product_list[i])
        listbox_products.delete(i)

def out_listbox():
    select = list(listbox_purchases.curselection())
    select.reverse()
    for i in select:
        listbox_products.insert(END, product_list[i])
        listbox_purchases.delete(i)

root= Tk()

listbox_products = Listbox(selectmode=EXTENDED)

for product in product_list:
    listbox_products.insert(END, product)

listbox_products.pack(side=LEFT)

f = Frame()
f.pack(side=LEFT)

btn_in = Button(f, text='>>>', command=in_listbox)
btn_out = Button(f, text='<<<', command=out_listbox)

btn_in.pack(side=TOP)
btn_out.pack(side=TOP)

listbox_purchases = Listbox(selectmode=EXTENDED)
listbox_purchases.pack(side=LEFT)

root.mainloop()